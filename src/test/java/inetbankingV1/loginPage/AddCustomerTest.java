package inetbankingV1.loginPage;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import inetbankingV1.actions.BaseClass;
import inetbankingV1.pageObjects.AddCustomerPage;
import inetbankingV1.pageObjects.LoginPage;

public class AddCustomerTest extends BaseClass
{
	@Test
	public void addNewCustomer() throws InterruptedException, IOException
	{
		LoginPage lp=new LoginPage(driver);
		lp.UserName(username);
		logger.info("user name provided");
		lp.Password(password);
		logger.info("password provided");
		lp.clickSubmit();
		Thread.sleep(5000);
		
		AddCustomerPage add=new AddCustomerPage(driver);
		add.clickAddNewCustomer();
		logger.info("providing customer details....");
		add.custName("Pavan");
		add.custgender("male");
		add.custdob("10","15","1985");
		Thread.sleep(5000);
		add.custadr("INDIA");
		add.custcity("HYD");
		add.custstate("AP");
		add.custpinno("5000074");
		add.custtelephoneno("987890091");
		
		String email=randomestring()+"@gmail.com";
		add.custemailid(email);
		add.custpassword("abcdef");
		add.custsubmit();
		
		

		
		Thread.sleep(3000);
		
		logger.info("validation started....");
		
		boolean res=driver.getPageSource().contains("Customer Registered Successfully!!!");
		
		if(res==true)
		{
			Assert.assertTrue(true);
			logger.info("test case passed....");
			
		}
		else
		{
			logger.info("test case failed....");
			captureScreen(driver,"addNewCustomer");
			Assert.assertTrue(false);
		}
			
	}

}
