package inetbankingV1.loginPage;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import inetbankingV1.actions.BaseClass;
import inetbankingV1.pageObjects.LoginPage;

public class LoginPageTest extends BaseClass
{
	
	@Test
	public void loginTest() throws IOException
	{
		logger.info("URL is opened");
		
		LoginPage lp=new LoginPage(driver);
		lp.UserName(username);
		logger.info("Entered username");
		
		lp.Password(password);
		logger.info("Entered password");
		
		lp.clickSubmit();
		
		
		/*lp.setPassword(password);
		logger.info("Entered password");
		
		lp.clickSubmit();
		*/
		
		if(driver.getTitle().equals("Guru99 Bank Manager HomePage"))
		{
			Assert.assertTrue(true);
			logger.info("Login test passed");
		}
		else
		{
			captureScreen(driver,"loginTest");
			Assert.assertTrue(false);
			logger.info("Login test failed");
		}
		
	}

}
