package inetbankingV1.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class LoginPage
{
	WebDriver driver;
	
	public LoginPage(WebDriver rdriver)
	{
		driver=rdriver;
	}
	
	
	
	
	By usernm=By.name("uid");
	public void UserName(String uname)
	{
		driver.findElement(usernm).sendKeys(uname);
	}
	
	By passwd=By.name("password");
	public void Password(String pwd)
	{
		driver.findElement(passwd).sendKeys(pwd);
	}
	
	By logn=By.name("btnLogin");
	public void clickSubmit()
	{
		driver.findElement(logn).click();
	}
	
	By logot=By.xpath("/html/body/div[3]/div/ul/li[15]/a");
	public void clickLogout()
	{
		driver.findElement(logot).click();
	}

	
/*WebDriver ldriver;
	
	public LoginPage(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
	}
		
	@FindBy(name="uid")
	@CacheLookup
	WebElement txtUserName;
	
	@FindBy(name="password")
	@CacheLookup
	WebElement txtPassword;
	
	@FindBy(name="btnLogin")
	@CacheLookup
	WebElement btnLogin;
	
	
	@FindBy(xpath="/html/body/div[3]/div/ul/li[15]/a")
	@CacheLookup
	WebElement lnkLogout;
	
	
	
	public void setUserName(String uname)
	{
		txtUserName.sendKeys(uname);
	}
	
	public void setPassword(String pwd)
	{
		txtPassword.sendKeys(pwd);
	}
	
	
	public void clickSubmit()
	{
		btnLogin.click();
	}	
	
	public void clickLogout()
	{
		lnkLogout.click();
	}
	*/
}
