package inetbankingV1.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddCustomerPage 
{
WebDriver driver;
	
	public AddCustomerPage(WebDriver rdriver)
	{
		driver=rdriver;
	}
	
	
	
	
	By addnew=By.xpath("/html/body/div[3]/div/ul/li[2]/a");
	public void clickAddNewCustomer()
	{
		driver.findElement(addnew).click();
	}
	
	By txtCustomerName=By.name("name");
	public void custName(String cname)
	{
		driver.findElement(txtCustomerName).sendKeys(cname);
	}
	
	By rdGender=By.name("rad1");
	public void custgender(String cgender)
	{
		driver.findElement(rdGender).click();
	}
	
	By txtdob=By.id("dob");
	public void custdob(String mm,String dd,String yy)
	{
		driver.findElement(txtdob).sendKeys(mm);
		driver.findElement(txtdob).sendKeys(dd);
		driver.findElement(txtdob).sendKeys(yy);
	}
	
	By txtaddress=By.name("addr");
	public void custadr(String cadr)
	{
		driver.findElement(txtaddress).sendKeys(cadr);
	}
	
	By txtcity=By.name("city");
	public void custcity(String ccity)
	{
		driver.findElement(txtcity).sendKeys(ccity);
	}
	
	By txtstate=By.name("state");
	public void custstate(String cstate)
	{
		driver.findElement(txtstate).sendKeys(cstate);
	}
	
	By txtpinno=By.name("pinno");
	public void custpinno(String cpinno) 
	{
		driver.findElement(txtpinno).sendKeys(cpinno);
	}
	
	By txttelephoneno=By.name("telephoneno");
	public void custtelephoneno(String ctelephoneno)
	{
		driver.findElement(txttelephoneno).sendKeys(ctelephoneno);
	}
	
	By txtemailid=By.name("emailid");
	public void custemailid(String cemailid)
	{
		driver.findElement(txtemailid).sendKeys(cemailid);
	}
	
	By txtpassword=By.name("password");
	public void custpassword(String cpassword)
	{
		driver.findElement(txtpassword).sendKeys(cpassword);
	}
	
	By btnSubmit=By.name("sub");
	public void custsubmit()
	{
		driver.findElement(btnSubmit).click();
	}
	

}
